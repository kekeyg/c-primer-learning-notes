#include <iostream>

int main()
{
    // 1-3 print hello 
    std::cout << "hello 1.3" << std::endl;

    // 1.4 
    std::cout << "Enter the numbers:" << std::endl;
    int v1 = 0, v2 = 0;
    std::cin >> v1 >> v2;
    std::cout << "the 积 of " << v1 << " " << v2 << " is " << v1*v2 <<std::endl;

    // 1.6
    std::cout << "程序片段不合法" << std::endl;
    // 修改后的
    std::cout << "the sum of " << v1;
    std::cout << " and " << v2;
    std::cout << " is " << v1 + v2 << std::endl;

    return 0;
}